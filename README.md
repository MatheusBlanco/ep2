Aprenda QEE

-Manual do usuário:
O programa foi criado como uma iniciativa entre os alunos do curso de Engenharia
de Energia e os alunos da Engenharia de Software. O aplicativo foi programado pa
ra que servisse como um simulador de determinados cálculos estudados pela enge-
nharia de energia, de modo a facilitar e incentivar o estudo de tais matérias.

Para fazer uso do aplicativo, baixe os arquivos e seus respectivos pacotes para 
a IDE. Depois, execute a Main do programa. A tela irá mostrar as opções de simu-
lação, e ao clicar no botão o usuário será levado para a tela da simulação. Nes-
ta tela, o usuario deverá inserir as respectivas informações pedidas pelo pro-
grama, e apertar nos botões "GO", a medida que for avançando. O programa então 
irá mostrar os dados calculados.

Este aplicativo foi criado na Eclipse IDE.