package model;

import java.util.ArrayList;
import java.util.List;

public class GeneralLogic{
	private double ang; double amp; List<Double> list = new ArrayList<>();
	
	public double getAng() {
		return ang;
	}
	
	public void setAng(double ang) {
		this.ang = ang;
	}

	public double getAmp() {
		return amp;
	}
	
	public void setAmp(double amp) {
		this.amp = amp;
	}
	
	public List<Double> getLista() {
		return list;
	}
	
	public void setLista(List<Double> lista) {
		this.list = lista;
	}
	
	public List<Double> Grafico(double amp, double ang){
		list.add(null);
		return list;	
	}
}
