package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;

import javax.swing.JPanel;

public class TrianguloPotencia<lineColor> extends JPanel {
	private static final long serialVersionUID = 1L;
	private int padding = 30; int jlabelPadding = 30; int PotAtiva; int PotReativa;
    private Color lineColor = new Color(44, 102, 230, 180); Color pointColor = new Color(100, 100, 100, 180);
    private static final Stroke GRAPH_STROKE = new BasicStroke(2f);
    
    public int getPadding() {
		return padding;
	}

	public void setPadding(int padding) {
		this.padding = padding;
	}

	public int getjlabelPadding() {
		return jlabelPadding;
	}

	public void setjlabelPadding(int jlabelPadding) {
		this.jlabelPadding = jlabelPadding;
	}

	public Color getLineColor() {
		return lineColor;
	}

	public void setLineColor(lineColor color) {
		this.lineColor = lineColor;
	}
	
	public Color getPointColor() {
		return pointColor;
	}

	public void setPointColor(Color pointColor) {
		this.pointColor = pointColor;
	}

	public static Stroke getGraphStroke() {
		return GRAPH_STROKE;
	}
    
    public TrianguloPotencia(int valor_potAtiva, int valor_potReativa) {
        this.PotAtiva = valor_potAtiva/100;
        this.PotReativa = valor_potReativa/100;
    }

    @Override
    protected void paintComponent(Graphics g) {
       super.paintComponent(g);
       Graphics2D g2 = (Graphics2D) g;
       g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

       g2.setColor(java.awt.Color.WHITE);
       g2.fillRect(50,0,360,300);
       g2.setColor(java.awt.Color.BLACK);

       g2.drawLine(50, 150,410, 150);
       g2.drawLine(230, 0, 230 , 300 );
       g2.setColor(java.awt.Color.green); 																
       g2.drawLine(230,150,230 + PotAtiva,150 );
       g2.setColor(java.awt.Color.red);
       g2.drawLine(230 + PotAtiva,150,230 + PotAtiva,150 - PotReativa);
       g2.setColor(java.awt.Color.blue);
       g2.drawLine(230,150,230 + PotAtiva,150 - PotReativa );      
   }
}

