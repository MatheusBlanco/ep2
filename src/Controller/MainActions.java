package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JPanel;

import view.UCII;

public class MainActions  implements ActionListener{	
	private JFrame startScreen; JPanel startPanel;

	private String conduct;
	
	public MainActions(JPanel startPanel, JFrame startScreen) {
		this.startScreen = startScreen;
		this.startPanel = startPanel;
	}

	public void actionPerformed(ActionEvent ev){
		setconduct(ev.getActionCommand());
		startPanel.setVisible(false);
			try {
				new UCII(startScreen);
				
			} catch (IOException ex) {
				System.out.println("Erro: não foi possivel criar a janela.\n");
			}
	 }

	public String getconduct() {
		return conduct;
	}

	public void setconduct(String conduct) {
		this.conduct = conduct;
	}
}
